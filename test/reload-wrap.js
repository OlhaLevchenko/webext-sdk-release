import "mocha/mocha.js";
import "mocha/mocha.css";

import "./mocha/mocha-setup-minimal.js";
import "./reload.js";
import "./mocha/mocha-runner.js";
